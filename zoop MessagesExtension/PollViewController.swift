//
//  PollViewController.swift
//  zoop MessagesExtension
//
//  Created by g e link on 7/24/18.
//  Copyright © 2018 g e link. All rights reserved.
//

import UIKit
import Messages

class PollViewController: UIViewController {
    weak var delegate: PollViewControllerDelegate?
    var isUnanswered: Bool = true
    var poll: Poll? {
        didSet {
            for view in self.view.subviews {
                view.removeFromSuperview()
            }
            if(isUnanswered) {
                renderPollOptions()
            } else {
                renderPollResults()
            }
        }
    }
    let colors = [
        UIColor(red: 175.0/255.0, green: 219.0/255.0, blue: 151.0/255.0, alpha: 1.0),
        UIColor(red: 198.0/255.0, green: 251.0/255.0, blue: 255.0/255.0, alpha: 1.0),
        UIColor(red: 249.0/255.0, green: 134.0/255.0, blue: 134.0/255.0, alpha: 1.0),
        UIColor(red: 246.0/255.0, green: 249.0/255.0, blue: 138.0/255.0, alpha: 1.0),
        UIColor(red: 73.0/255.0, green: 74.0/255.0, blue: 91.0/255.0, alpha: 1.0)
    ]
    
    func renderPollOptions() {
        guard let unwrappedPoll = poll else { return }
        // add question label
        let label = UILabel(frame: CGRect(x: 10, y: 6, width: self.view.bounds.width - 20, height: 60))
        label.center = CGPoint(x: self.view.center.x, y: 30.0)
        label.textAlignment = .center
        label.text = poll?.question
        label.textColor = .white
        label.numberOfLines = 3
        label.font =  UIFont(name: "HelveticaNeue-Bold", size: 18)!
        
        self.view.addSubview(label)
        
        var index = 0
        // add option questions
        for option in unwrappedPoll.options! {
            let button = UIButton(frame: CGRect(x: 10, y: 0, width: self.view.bounds.width - 20, height: 64))
            let centerY = 100 + 70 * index
            button.center = CGPoint(x: self.view.center.x, y: CGFloat(centerY))
            button.setTitle(option, for: .normal)
            button.setTitleColor(colors[4], for: .normal)
            button.titleLabel?.font =  UIFont(name: "HelveticaNeue-Bold", size: 18)!
            button.layer.borderColor = colors[index].cgColor
            button.layer.backgroundColor = colors[index].cgColor
            button.layer.borderWidth = 4.0
            button.layer.cornerRadius = 12.0
            
            button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
            self.view.addSubview(button)
            index += 1
        }
    }
    
    func renderPollResults() {
        // guard let unwrappedPoll = poll else { return }
        // add question label
        let label = UILabel(frame: CGRect(x: 10, y: 6, width: self.view.bounds.width - 20, height: 60))
        label.center = CGPoint(x: self.view.center.x, y: 20.0)
        label.textAlignment = .center
        label.text = poll?.question
        label.textColor = .white
        label.numberOfLines = 3
        label.font =  UIFont(name: "HelveticaNeue-Bold", size: 18)!
        
        let img = delegate?.createResultImage(self, results: (poll?.results)!, optionsText: (poll?.options)!)
        let resultsView = UIImageView(image: img)
        resultsView.frame = CGRect(x: 0, y: 70, width: 200, height: 200)
        resultsView.center = CGPoint(x: self.view.center.x, y: 160.0)
        
        let warningLabel = UILabel(frame: CGRect(x: 10, y: 340, width: self.view.bounds.width - 20, height: 60))
        warningLabel.textAlignment = .center
        warningLabel.center = CGPoint(x: self.view.center.x, y: 300)
        warningLabel.text = "Woops, you've already voted. No ballet stuffing allowed!"
        warningLabel.textColor = .white
        warningLabel.numberOfLines = 3
        warningLabel.font =  UIFont(name: "HelveticaNeue-Thin", size: 12)!
        
        
        self.view.addSubview(label)
        self.view.addSubview(resultsView)
        self.view.addSubview(warningLabel)
    }
    
    @objc
    func buttonAction(sender: UIButton) {
        delegate?.buttonAction(self, clickedTitle: sender.currentTitle!, poll: &self.poll!)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

protocol PollViewControllerDelegate: class {
    func buttonAction(_ controller: PollViewController, clickedTitle: String, poll: inout Poll)
    func createResultImage(_ controller: PollViewController, results: Results, optionsText: [String]) -> UIImage
}
