//
//  Poll.swift
//  zoop MessagesExtension
//
//  Created by g e link on 6/28/18.
//  Copyright © 2018 g e link. All rights reserved.
//

import Foundation
import Messages

struct Results {
    var totalAnswers = 0
    var optionTallies: [Int]
}

struct Poll {
    var question: String?
    var options: [String]?
    var participantIds: [UUID] = [UUID]()
    var results = Results(totalAnswers: 0, optionTallies: [Int]())
    var isStarted = false
}

extension Poll {
    var queryItems: [URLQueryItem] {
        let items = [URLQueryItem]()
        return items
    }

    init?(queryItems: [URLQueryItem]) {
        for queryItem in queryItems {
            guard let value = queryItem.value else { continue }
            if queryItem.name == "question" {
                self.question = value
                self.isStarted = true
            }
            if queryItem.name == "options" {
                self.options = parseOptions(optionsBlob: value)
            }
            if queryItem.name == "results" {
                self.results = parseResults(results: value, optionCount: self.options?.count ?? 0)
            }
            if queryItem.name == "participantIds" {
                self.participantIds = parseParticipantIds(idString: value)
            }
            print("qi: \(queryItem)")
            
        }
    }
    
    private func parseOptions(optionsBlob: String) -> [String] {
        var options = [String]()
        for o in optionsBlob.split(separator: "\n") {
            options.append(String(o))
        }
        return options
    }

    private func parseResults(results: String, optionCount: Int) -> Results {
        var total = 0
        var tallies = [Int]()
        for e in results.split(separator: "&") {
            let keyVal = e.split(separator: "=")
            if (keyVal.count > 1 && keyVal[0] == "total") {
                total = Int(keyVal[1])!
            }
            if (keyVal[0] == "tallies") {
                if(keyVal.count > 1) {
                    for val in keyVal[1].split(separator: ",") {
                        tallies.append(Int(val)!)
                    }
                } else  {
                    tallies = Array(repeating: 0, count: optionCount)
                }
            }
        }
        return Results(totalAnswers: total, optionTallies: tallies)
    }
    
    private func parseParticipantIds(idString: String) -> [UUID] {
        var ids = [UUID]()
        for id in idString.split(separator: ",") {
            let uuid = UUID.init(uuidString: String(id))
            ids.append(uuid!)
        }
        return ids
    }
}

extension Poll {
    init?(message: MSMessage?) {
        guard let messageURL = message?.url else { return nil }
        guard let urlComponents = NSURLComponents(url: messageURL, resolvingAgainstBaseURL: false), let queryItems =
            urlComponents.queryItems else { return nil }
        
        self.init(queryItems: queryItems)
    }
}
