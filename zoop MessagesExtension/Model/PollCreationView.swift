//
//  PollCreationView.swift
//  zoop MessagesExtension
//
//  Created by g e link on 7/20/18.
//  Copyright © 2018 g e link. All rights reserved.
//

import UIKit
import Foundation
class PollCreationView : UIView {
    weak var delegate: PollCreationDelegate?

    let scrollView: UIScrollView = {
        let view = UIScrollView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .cyan
        return view
    }()

    private let sendButton: Butt = {
        let button = Butt(frame: CGRect(x: 0, y: 0, width: 100, height: 64))
        button.setTitle("SEND POLL", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.cornerRadius = 0
        button.addTarget(self, action: #selector(sendPoll(_:)), for: .touchUpInside)
        button.layer.backgroundColor = UIColor(red: 73.0/255.0, green: 74.0/255.0, blue: 91.0/255.0, alpha: 1.0).cgColor
        button.titleLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 18)!
        button.translatesAutoresizingMaskIntoConstraints = false
        
        return button
    }()

    private let questionLabel: UILabel = {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        label.text = "The question:"
        label.textColor = UIColor(red: 73.0/255.0, green: 74.0/255.0, blue: 91.0/255.0, alpha: 1.0)
        label.textAlignment = .center
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 18)!
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    let questionInput: TextBox = {
        let input = TextBox(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        input.backgroundColor = .white
        input.borderColor = 4
        input.placeholder = "Some funny joke question"
        input.translatesAutoresizingMaskIntoConstraints = false
        return input
    }()

    private let answerLabel: UILabel = {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        label.text = "The options:"
        label.textColor = UIColor(red: 73.0/255.0, green: 74.0/255.0, blue: 91.0/255.0, alpha: 1.0)
        label.textAlignment = .center
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 18)!
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private let initialInput: TextBox = {
        let input = TextBox(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        input.backgroundColor = .white
        input.placeholder = "A silly answer"
        input.translatesAutoresizingMaskIntoConstraints = false
        return input
    }()

    private let addInputButton: Butt = {
        let button = Butt(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        button.setTitle("add another option", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(addInput(_:)), for: .touchUpInside)
        button.layer.backgroundColor = UIColor(red: 73.0/255.0, green: 74.0/255.0, blue: 91.0/255.0, alpha: 1.0).cgColor
        button.titleLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 14)!
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    private let disclaimer: UILabel = {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        label.text = "Divide your pals!"
        label.textColor = UIColor(red: 73.0/255.0, green: 74.0/255.0, blue: 91.0/255.0, alpha: 1.0)
        label.textAlignment = .center
        label.font = UIFont(name: "HelveticaNeue-Thin", size: 14)!
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    private let noMoreLabel: UILabel = {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        label.text = "You've hit the option limit"
        label.textColor = UIColor(red: 73.0/255.0, green: 74.0/255.0, blue: 91.0/255.0, alpha: 1.0)
        label.textAlignment = .center
        label.font = UIFont(name: "HelveticaNeue-Thin", size: 14)!
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    var bumpingElements = [UIView]()
    private var bumpingAnchors = [NSLayoutConstraint]()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        bumpingElements.append(addInputButton)
        bumpingElements.append(initialInput)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    func setupViews() {
        // add the scroll view to self.view
        self.addSubview(sendButton)
        self.addSubview(scrollView)
        let buttonHeight = 64
        // pin send button to bottom of screen (will shift for keyboard)
        sendButton.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 16.0).isActive = true
        sendButton.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        sendButton.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 0).isActive = true
        sendButton.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -64).isActive = true
        sendButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0).isActive = true
        sendButton.heightAnchor.constraint(equalToConstant: CGFloat(buttonHeight)).isActive = true

        // constrain the scroll view to 8-pts on each side
        scrollView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 0).isActive = true
        scrollView.topAnchor.constraint(equalTo: self.topAnchor, constant: CGFloat(buttonHeight)).isActive = true
        scrollView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -120).isActive = true // should be bottom bar spacing whatever that is

        scrollView.addSubview(questionLabel)
        questionLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 16.0).isActive = true
        questionLabel.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 16.0).isActive = true

        scrollView.addSubview(questionInput)
        questionInput.leftAnchor.constraint(equalTo: scrollView.leftAnchor, constant: 16.0).isActive = true
        questionInput.topAnchor.constraint(equalTo: questionLabel.bottomAnchor, constant: 16.0).isActive = true
        questionInput.rightAnchor.constraint(equalTo: scrollView.rightAnchor, constant: -16.0).isActive = true
        questionInput.heightAnchor.constraint(equalToConstant: 48).isActive = true

        scrollView.addSubview(answerLabel)
        answerLabel.leftAnchor.constraint(equalTo: scrollView.leftAnchor, constant: 16.0).isActive = true
        answerLabel.topAnchor.constraint(equalTo: questionInput.bottomAnchor, constant: 16.0).isActive = true

        scrollView.addSubview(initialInput)
        initialInput.leftAnchor.constraint(equalTo: scrollView.leftAnchor, constant: 16.0).isActive = true
        let textBump = initialInput.topAnchor.constraint(equalTo: answerLabel.bottomAnchor, constant: 16.0)
        textBump.isActive = true
        initialInput.rightAnchor.constraint(equalTo: scrollView.rightAnchor, constant: -16.0).isActive = true
        initialInput.heightAnchor.constraint(equalToConstant: 48).isActive = true

        scrollView.addSubview(addInputButton)
        addInputButton.leftAnchor.constraint(equalTo: scrollView.leftAnchor, constant: 16.0).isActive = true
        let inputBump = addInputButton.topAnchor.constraint(equalTo: initialInput.bottomAnchor, constant: 16.0)
        inputBump.isActive = true
        addInputButton.rightAnchor.constraint(equalTo: scrollView.rightAnchor, constant: -16.0).isActive = true

        scrollView.addSubview(disclaimer)
        disclaimer.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 40.0).isActive = true
        disclaimer.topAnchor.constraint(equalTo: addInputButton.bottomAnchor, constant: 200).isActive = true
        disclaimer.rightAnchor.constraint(equalTo: scrollView.rightAnchor, constant: -16.0).isActive = true
        disclaimer.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: -16.0).isActive = true
        disclaimer.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        
        bumpingAnchors.append(inputBump)
        bumpingAnchors.append(textBump)
    }
    
    @objc private func sendPoll(_ sender: UIButton?) {
        var poll = Poll()
        var pollOptions = [String]()
        
        for option in bumpingElements[1...bumpingElements.count-1] {
            let optionBox = option as! TextBox
            if optionBox.text != nil && !optionBox.text!.isEmpty {
                pollOptions.append(optionBox.text!)
            }
        }
        
        poll.question = questionInput.text!
        poll.isStarted = true
        poll.options = pollOptions

        delegate?.sendPoll(poll: poll)
    }
    
    @objc private func addInput(_ sender: UIButton?) {

        let count = bumpingElements.count
        if(count < 5) {
            var aboveElement = answerLabel as UIView

            let newElement = TextBox(frame: CGRect(x: 0, y: 0, width: 100, height: 48))
            newElement.backgroundColor = .white
            newElement.borderColor = 4
            newElement.placeholder = "A different silly answer \(count)"
            newElement.translatesAutoresizingMaskIntoConstraints = false
            
            scrollView.addSubview(newElement)
            newElement.leftAnchor.constraint(equalTo: scrollView.leftAnchor, constant: 16.0).isActive = true
            let newBump = newElement.topAnchor.constraint(equalTo: aboveElement.topAnchor, constant: aboveElement.frame.height + 16)
            newBump.isActive = true
            newElement.rightAnchor.constraint(equalTo: scrollView.rightAnchor, constant: -16.0).isActive = true
            newElement.heightAnchor.constraint(equalToConstant: 48).isActive = true

            //focus this element
            newElement.becomeFirstResponder()

            bumpingAnchors.append(newBump)
            bumpingElements.append(newElement)

            for i in bumpingAnchors.indices.reversed() {
                let element = bumpingElements[i]
                bumpingAnchors[i].isActive = false
                bumpingAnchors[i] = element.topAnchor.constraint(equalTo: aboveElement.topAnchor, constant: aboveElement.frame.height + 16)
                bumpingAnchors[i].isActive = true
                
                aboveElement = element
            }
        }
        if (count == 4) {
            // add text saying no more
            scrollView.addSubview(noMoreLabel)
            noMoreLabel.leftAnchor.constraint(equalTo: scrollView.leftAnchor, constant: 16.0).isActive = true
            noMoreLabel.topAnchor.constraint(equalTo: bumpingElements[1].bottomAnchor, constant: 16.0).isActive = true
            noMoreLabel.rightAnchor.constraint(equalTo: scrollView.rightAnchor, constant: -16.0).isActive = true

            // remove button
            if (addInputButton.isDescendant(of: scrollView)) {
                addInputButton.isHidden = true
            }
        }
    }
}

protocol PollCreationDelegate: class {
    func sendPoll(poll: Poll)
}
