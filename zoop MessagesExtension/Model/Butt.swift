//
//  Butt.swift
//  zoop MessagesExtension
//
//  Created by g e link on 7/10/18.
//  Copyright © 2018 g e link. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class Butt: UIButton {
    @IBInspectable var cornerRadius: CGFloat = 12 {
        didSet {
            refreshCorners(value: cornerRadius)
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 4 {
        didSet {
            refreshBorderWidth(value: borderWidth)
        }
    }

    @IBInspectable var borderColor: CGColor = UIColor(red: 73.0/255.0, green: 74.0/255.0, blue: 91.0/255.0, alpha: 1.0).cgColor {
        didSet {
            refreshBorderColor(value: borderColor)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }
    
    override func prepareForInterfaceBuilder() {
        sharedInit()
    }
    
    func sharedInit() {
        refreshCorners(value: cornerRadius)
        refreshBorderWidth(value: borderWidth)
        refreshBorderColor(value: borderColor)
    }
    
    func refreshCorners(value: CGFloat) {
        layer.cornerRadius = value
    }
    
    func refreshBorderWidth(value: CGFloat) {
        layer.borderWidth = value
    }

    func refreshBorderColor(value: CGColor) {
        layer.borderColor = value
    }
}
