//
//  Butt.swift
//  zoop MessagesExtension
//
//  Created by g e link on 7/10/18.
//  Copyright © 2018 g e link. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class TextBox: UITextField {
    let padding = UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 12)
    @IBInspectable var cornerRadius: CGFloat = 6 {
        didSet {
            refreshCorners(value: cornerRadius)
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 4 {
        didSet {
            refreshBorderWidth(value: borderWidth)
        }
    }
    
    @IBInspectable var borderColor: CGFloat = 0 {
        didSet {
            refreshBorderColor(value: borderColor)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }
    
    override func prepareForInterfaceBuilder() {
        sharedInit()
    }
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    func sharedInit() {
        refreshCorners(value: cornerRadius)
        refreshBorderWidth(value: borderWidth)
        refreshBorderColor(value: borderColor)
    }
    
    func refreshCorners(value: CGFloat) {
        layer.cornerRadius = value
    }
    
    func refreshBorderWidth(value: CGFloat) {
        layer.borderWidth = value
    }
    
    func refreshBorderColor(value: CGFloat) {
        var color: CGColor
        switch value {
        case 0:
            // granny smith
            color = UIColor(red: 175.0/255.0, green: 219.0/255.0, blue: 151.0/255.0, alpha: 1.0).cgColor
        case 1:
            // diamond
            color = UIColor(red: 198.0/255.0, green: 251.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
        case 2:
            // tulip
            color = UIColor(red: 249.0/255.0, green: 134.0/255.0, blue: 134.0/255.0, alpha: 1.0).cgColor
        case 3:
            // vanilla
            color = UIColor(red: 246.0/255.0, green: 249.0/255.0, blue: 138.0/255.0, alpha: 1.0).cgColor
        default:
            // quartz
            color = UIColor(red: 73.0/255.0, green: 74.0/255.0, blue: 91.0/255.0, alpha: 1.0).cgColor
        }
        
        layer.borderColor = color
    }
}
