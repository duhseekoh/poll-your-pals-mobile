//
//  ExpandedViewController.swift
//  zoop MessagesExtension
//
//  Created by g e link on 7/24/18.
//  Copyright © 2018 g e link. All rights reserved.
//

import UIKit
import Messages

class ExpandedViewController: UIViewController, UITextFieldDelegate, PollCreationDelegate {
    
    weak var delegate: ExpandedViewControllerDelegate?
    var poll: Poll?
    var pollCreationView: PollCreationView? = nil
    
    func sendPoll(poll: Poll) {
        self.poll = poll
        delegate?.pollButtTouched(self, poll: poll)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pollCreationView = PollCreationView(frame: self.view.bounds)
        self.view = pollCreationView
        pollCreationView?.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: Notification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: Notification.Name.UIKeyboardWillShow, object: nil)
        self.hideKeyboard()
    }

    @objc func keyboardWillAppear(notification: NSNotification) {
        //Do something here
        if let keyBoardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if let scroll = pollCreationView?.scrollView {
                var contentInset: UIEdgeInsets = scroll.contentInset
                contentInset.bottom = keyBoardSize.size.height
                scroll.contentInset = contentInset
            }
        }
    }
    
    @objc func keyboardWillDisappear(notification: NSNotification) {
        //Do something here
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            if let scroll = pollCreationView?.scrollView {
                let contentInset = UIEdgeInsets.zero
                scroll.contentInset = contentInset
            }
        }
    }
    
    //    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    //        print(textField.tag)
    //        let nextTag = textField.tag + 1
    //        // Try to find next responder
    //        let nextResponder = textField.superview?.viewWithTag(nextTag) as UIResponder!
    //
    //        if nextResponder != nil {
    //            // Found next responder, so set it
    //            nextResponder?.becomeFirstResponder()
    //        } else {
    //            // Not found, so remove keyboard
    //            textField.resignFirstResponder()
    //        }
    //
    //        return true
    //    }
    
    func hideKeyboard() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

protocol ExpandedViewControllerDelegate: class {
    func pollButtTouched(_ controller: ExpandedViewController, poll: Poll)
}
