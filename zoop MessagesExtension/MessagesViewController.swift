//
//  MessagesViewController.swift
//  zoop MessagesExtension
//
//  Created by g e link on 6/25/18.
//  Copyright © 2018 g e link. All rights reserved.
//

import UIKit
import Messages

class MessagesViewController: MSMessagesAppViewController, ExpandedViewControllerDelegate, CompactViewControllerDelegate, PollViewControllerDelegate {
    // var newPoll: Bool = true
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let expandedVC = ExpandedViewController(nibName: nil, bundle: nil)
        let compactVC = CompactViewController(nibName: nil, bundle: nil)
        let pollVC = PollViewController(nibName: nil, bundle: nil)
        
        self.addChildViewController(expandedVC)
        self.addChildViewController(compactVC)
        self.addChildViewController(pollVC)
        
        self.view.addSubview(expandedVC.view)
        self.view.addSubview(compactVC.view)
        self.view.addSubview(pollVC.view)
        
        expandedVC.didMove(toParentViewController: self)
        compactVC.didMove(toParentViewController: self)
        pollVC.didMove(toParentViewController: self)
    }
    
    private func isSenderSameAsRecipient() -> Bool {
        guard let conversation = activeConversation else { return false }
        guard let message = conversation.selectedMessage else { return false }
        
        return message.senderParticipantIdentifier == conversation.localParticipantIdentifier
    }
    
    func createResultImage(results: Results, optionsText: [String]) -> UIImage {
        let renderer = UIGraphicsImageRenderer(size: CGSize(width: 200, height: 200))
        let radius = 58
        var startAngle = -CGFloat.pi * 0.5
        let colors = [
            UIColor(red: 175.0/255.0, green: 219.0/255.0, blue: 151.0/255.0, alpha: 1.0),
            UIColor(red: 198.0/255.0, green: 251.0/255.0, blue: 255.0/255.0, alpha: 1.0),
            UIColor(red: 249.0/255.0, green: 134.0/255.0, blue: 134.0/255.0, alpha: 1.0),
            UIColor(red: 246.0/255.0, green: 249.0/255.0, blue: 138.0/255.0, alpha: 1.0),
            UIColor(red: 73.0/255.0, green: 74.0/255.0, blue: 91.0/255.0, alpha: 1.0)
        ]
        let viewCenter = CGPoint(x: 100, y: 70)
        var y = 136
        let img = renderer.image { ctx in
            var index = 0
            if (results.totalAnswers <= 0) {
                return
            }
            let rectangle = CGRect(x: 0, y: 0,  width: 200, height: 200)
            ctx.cgContext.setFillColor(colors[4].cgColor)
            ctx.cgContext.setStrokeColor(colors[4].cgColor)
            ctx.cgContext.setLineWidth(10)
            ctx.cgContext.addRect(rectangle)
            ctx.cgContext.drawPath(using: .fillStroke)
            
            for option in results.optionTallies {
                ctx.cgContext.setFillColor(colors[index].cgColor)
                let endAngle = startAngle + (2 * CGFloat.pi * CGFloat(option) / CGFloat(results.totalAnswers))
                print("start - end : \(startAngle) - \(endAngle)")
                ctx.cgContext.move(to: viewCenter)
                
                ctx.cgContext.addArc(center: viewCenter, radius: CGFloat(radius), startAngle: startAngle, endAngle: endAngle, clockwise: false)
                ctx.cgContext.addRect(CGRect(x: 10, y: y, width: 10, height: 10))
                ctx.cgContext.drawPath(using: .fill)
                let attrs: [NSAttributedStringKey: Any] = [
                    .foregroundColor : colors[index],
                    .font : UIFont(name: "HelveticaNeue-Bold", size: 14)!
                ]
                
                optionsText[index].draw(with: CGRect(x: 24, y: y-4, width: 200, height: 200), options: .usesLineFragmentOrigin, attributes: attrs, context: nil)
                startAngle = endAngle
                index = index + 1
                y = y + 16
            }
        }
        
        return img
    }
    
    func createPollImage(options: [String]) -> UIImage {
        let renderer = UIGraphicsImageRenderer(size: CGSize(width: 200, height: 200))
        let colors = [
            UIColor(red: 175.0/255.0, green: 219.0/255.0, blue: 151.0/255.0, alpha: 1.0),
            UIColor(red: 198.0/255.0, green: 251.0/255.0, blue: 255.0/255.0, alpha: 1.0),
            UIColor(red: 249.0/255.0, green: 134.0/255.0, blue: 134.0/255.0, alpha: 1.0),
            UIColor(red: 246.0/255.0, green: 249.0/255.0, blue: 138.0/255.0, alpha: 1.0),
            UIColor(red: 73.0/255.0, green: 74.0/255.0, blue: 91.0/255.0, alpha: 1.0)
        ]
        let img = renderer.image { ctx in
            let rectangle = CGRect(x: 0, y: 0,  width: 200, height: 200)
            ctx.cgContext.setFillColor(colors[4].cgColor)
            ctx.cgContext.setStrokeColor(colors[4].cgColor)
            ctx.cgContext.setLineWidth(10)
            ctx.cgContext.addRect(rectangle)
            ctx.cgContext.drawPath(using: .fillStroke)
            var index = 0;
            var y = 30
            for option in options {
                let attrs: [NSAttributedStringKey: Any] = [
                    .foregroundColor : colors[index],
                    .font : UIFont(name: "HelveticaNeue-Bold", size: 18)!
                ]
                option.draw(with: CGRect(x: 40, y: y, width: 200, height: 200), options: .usesLineFragmentOrigin, attributes: attrs, context: nil)
                index = index + 1
                y = y + 22
            }
        }
        
        return img
    }
    
    func createQueryString(options: [String]) -> String {
        var caption = ""
        for option in options {
            caption.append("\(option)\n")
        }
        return caption
    }

    // clicked on the create poll button
    func clickertonClicks(poll: Poll) {
        let conversation = activeConversation
        let session = conversation?.selectedMessage?.session ?? MSSession()
        let layout = MSMessageTemplateLayout()
        var optionQueryString: String = ""

        if poll.options == nil || poll.options!.count < 2 {
            let refreshAlert = UIAlertController(title: "Slow down", message: "What's the point of a poll with no choice?", preferredStyle: UIAlertControllerStyle.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                print("Handle Ok logic here")
            }))
            
            present(refreshAlert, animated: true, completion: nil)
        }
        else {
            if let options = poll.options {
                layout.image = createPollImage(options: options) // prolly need a backup image here
                optionQueryString = createQueryString(options: options)
            }
            layout.caption = poll.question

            let message = MSMessage(session: session)
            message.layout = layout
            
            var components = URLComponents()
            
            components.queryItems = [
                URLQueryItem(name: "question", value: poll.question),
                URLQueryItem(name: "options", value: optionQueryString),
                URLQueryItem(name: "results", value: stringifyResults(results: poll.results))
            ]
            message.url = components.url
            message.summaryText = "Created a poll"

            activeConversation?.insert(message)
            dismiss()
        }
    }
    
    // clicked on a poll option
    func buttonAction(title: String, poll: inout Poll) {
        let conversation = activeConversation
        let session = conversation?.selectedMessage?.session ?? MSSession()
        let layout = MSMessageTemplateLayout()
        
        let index = poll.options?.index(of: title)
        poll.results.optionTallies[index!] += 1
        poll.results.totalAnswers += 1
        poll.participantIds.append((conversation?.localParticipantIdentifier)!)

        layout.image = createResultImage(results: poll.results, optionsText: poll.options ?? [])
        layout.caption = poll.question
        
        let message = MSMessage(session: session)
        message.layout = layout
        
        var components = URLComponents()
        
        var optionString = ""
        for option in poll.options! {
            optionString += "\(option)\n"
        }
        
        components.queryItems = [
            URLQueryItem(name: "question", value: poll.question),
            URLQueryItem(name: "options", value: optionString),
            URLQueryItem(name: "results", value: stringifyResults(results: poll.results)),
            URLQueryItem(name: "participantIds", value: stringifyUUIDs(uuids: poll.participantIds))
        ]
        message.url = components.url
        message.summaryText = "Voted for \(title)"
        
        activeConversation?.insert(message)
        dismiss()
    }

    private func stringifyUUIDs(uuids: [UUID]) -> String {
        var uuidString = ""
        for id in uuids {
            uuidString += "\(id),"
        }
        return uuidString
    }
    private func stringifyResults(results: Results) -> String {
        var resultsString = "total=\(results.totalAnswers)&tallies="
        
        for option in results.optionTallies {
            resultsString += "\(option),"
        }
        return resultsString;
    }

    private func presentViewController(for conversation: MSConversation, with presentationStyle: MSMessagesAppPresentationStyle) {
        removeAllChildViewControllers()
//        print("convo message \(String(describing: conversation.selectedMessage))")
        let controller: UIViewController
        if presentationStyle == .compact {
            let poll = Poll(message: conversation.selectedMessage) ?? nil
            controller = instantiateCompactView(with: poll)
        } else {
            let poll = Poll(message: conversation.selectedMessage) ?? Poll()
            if poll.isStarted {
                let isUnanswered = poll.participantIds.index(of: conversation.localParticipantIdentifier) == nil
                controller = instantiatePollView(with: poll, isUnanswered: isUnanswered)
//                print("unanswered? \(isUnanswered)")
//                print("pollllllll   \(poll)")
//                print("pIds: \(poll.participantIds)")
            }
            else {
                controller = instantiateExpandedView(with: poll)
            }
        }
        
        addChildViewController(controller)
        view.addSubview(controller.view)
        controller.didMove(toParentViewController: self)
    }
    
    private func instantiateCompactView(with poll: Poll?) -> UIViewController {
        guard let controller = storyboard?.instantiateViewController(withIdentifier: "compactVC") as? CompactViewController else {
            fatalError("Can't instantiate Compact View")
        }
        controller.delegate = self
        controller.poll = poll

        return controller
    }
    
    private func instantiateExpandedView(with poll: Poll) -> UIViewController {
        guard let controller = storyboard?.instantiateViewController(withIdentifier: "expandedVC") as? ExpandedViewController else {
            fatalError("Can't instantiate Expanded View")
        }
        controller.delegate = self
        controller.poll = poll
        return controller
    }
    
    private func instantiatePollView(with poll: Poll, isUnanswered: Bool) -> UIViewController {
        guard let controller = storyboard?.instantiateViewController(withIdentifier: "pollVC") as? PollViewController else {
            fatalError("Can't instantiate Poll View")
        }
        controller.delegate = self
        
        controller.isUnanswered = isUnanswered
        controller.poll = poll
        return controller
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Conversation Handling
    
    override func willBecomeActive(with conversation: MSConversation) {
        // Called when the extension is about to move from the inactive to active state.
        // This will happen when the extension is about to present UI.
        
        // Use this method to configure the extension and restore previously stored state.
        super.willBecomeActive(with: conversation)
        presentViewController(for: conversation, with: presentationStyle)
    }
    
    override func didResignActive(with conversation: MSConversation) {
        // Called when the extension is about to move from the active to inactive state.
        // This will happen when the user dissmises the extension, changes to a different
        // conversation or quits Messages.
        
        // Use this method to release shared resources, save user data, invalidate timers,
        // and store enough state information to restore your extension to its current state
        // in case it is terminated later.
    }
   
    override func didReceive(_ message: MSMessage, conversation: MSConversation) {
        // Called when a message arrives that was generated by another instance of this
        // extension on a remote device.
        
        // Use this method to trigger UI updates in response to the message.
    }
    
    override func didStartSending(_ message: MSMessage, conversation: MSConversation) {
        // Called when the user taps the send button.
    }
    
    override func didCancelSending(_ message: MSMessage, conversation: MSConversation) {
        // Called when the user deletes the message without sending it.
    
        // Use this to clean up state related to the deleted message.
    }
    
    override func willTransition(to presentationStyle: MSMessagesAppPresentationStyle) {
        // Called before the extension transitions to a new presentation style.
    
        // Use this method to prepare for the change in presentation style.
        guard let conversation = activeConversation else {
            fatalError("Expected a conversation")
        }
        presentViewController(for: conversation, with: presentationStyle)
    }
    
    override func didTransition(to presentationStyle: MSMessagesAppPresentationStyle) {
        // Called after the extension transitions to a new presentation style.
    
        // Use this method to finalize any behaviors associated with the change in presentation style.
    }

    private func removeAllChildViewControllers() {
        for child in childViewControllers {
            child.willMove(toParentViewController: nil)
            child.view.removeFromSuperview()
            child.removeFromParentViewController()
        }
    }
    
    // compact delegate func
    func compactButtTouched(_ controller: CompactViewController) {
//        self.newPoll = true
        requestPresentationStyle(.expanded)
    }

    // expanded delegate func
    func pollButtTouched(_ controller: ExpandedViewController, poll: Poll) {
        clickertonClicks(poll: poll)
    }

    // pollview delegate func
    func createResultImage(_ controller: PollViewController, results: Results, optionsText: [String]) -> UIImage {
        return createResultImage(results: results, optionsText: optionsText)
    }
    
    func buttonAction(_ controller: PollViewController, clickedTitle: String, poll: inout Poll) {
        buttonAction(title: clickedTitle, poll: &poll)
    }
}
