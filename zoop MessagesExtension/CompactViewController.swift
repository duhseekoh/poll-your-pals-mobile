//
//  CompactViewController.swift
//  zoop MessagesExtension
//
//  Created by g e link on 7/24/18.
//  Copyright © 2018 g e link. All rights reserved.
//

import UIKit
import Messages

class CompactViewController: UIViewController {
    @IBOutlet weak var pollButt: UIButton!
    weak var delegate: CompactViewControllerDelegate?
    var poll: Poll?
    
    @IBAction func touchTheButt(_ sender: Any) {
        delegate?.compactButtTouched(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(poll != nil) {
            pollButt.setTitle("VIEW RESULTS",for: .normal)
            // TODO: add button to create new poll
        }
    }
}

protocol CompactViewControllerDelegate: class {
    func compactButtTouched(_ controller: CompactViewController)
}
